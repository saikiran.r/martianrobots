package com.codingchallenge.martianrobots.model;

import org.junit.Assert;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CoordinatesTest {

    @Test
    public void shouldCreateCoordinates(){
        long x = 100000;
        long y = 1000000;
        Coordinates coordinates = new Coordinates(x, y);
        assertThat(coordinates.getX()).isEqualTo(x);
        assertThat(coordinates.getY()).isEqualTo(y);
    }

    @Test
    public void shouldGetTrue_WhenTriggeringcheckIfInsideWorldFunc_GivenAValidCoordinates(){

        Coordinates worldVertex = new Coordinates(2, 3);
        Coordinates position = new Coordinates(1,1);
        assertThat(position.checkIfInsideWorld(worldVertex)).isTrue();

        position = new Coordinates(2,1);
        assertThat(position.checkIfInsideWorld(worldVertex)).isTrue();

        position = new Coordinates(0,0);
        assertThat(position.checkIfInsideWorld(worldVertex)).isTrue();

        position = new Coordinates(2,3);
        assertThat(position.checkIfInsideWorld(worldVertex)).isTrue();
    }

    @Test
    public void shouldGetFalse_WhenTriggeringcheckIfInsideWorldFunc_GivenAInValidCoordinates(){

        Coordinates worldVertex = new Coordinates(2, 3);
        Coordinates position = new Coordinates(-1,0);
        assertThat(position.checkIfInsideWorld(worldVertex)).isFalse();

        position = new Coordinates(0,-1);
        assertThat(position.checkIfInsideWorld(worldVertex)).isFalse();

        position = new Coordinates(3,0);
        assertThat(position.checkIfInsideWorld(worldVertex)).isFalse();

        position = new Coordinates(3,3);
        assertThat(position.checkIfInsideWorld(worldVertex)).isFalse();

        position = new Coordinates(2,4);
        assertThat(position.checkIfInsideWorld(worldVertex)).isFalse();

        position = new Coordinates(1,4);
        assertThat(position.checkIfInsideWorld(worldVertex)).isFalse();

        position = new Coordinates(-1,-1);
        assertThat(position.checkIfInsideWorld(worldVertex)).isFalse();
    }

}
