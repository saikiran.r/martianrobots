package com.codingchallenge.martianrobots.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RobotControllerTest {

    @Autowired
	private TestRestTemplate restTemplate;

    @Test
    public void shouldPass_whenRequestBodyIsASuccessScenario(){
        String input = "5 3\n" +
                "1 1 E\n" +
                "RFRFRFRF\n" +
                "3 2 N\n" +
                "FRRFLLFFRRFLL\n" +
                "0 3 W\n" +
                "LLFFFLFLFL";
        String output = "1 1 E\n" +
                "3 3 N LOST\n" +
                "2 3 S";
        ResponseEntity<String> responseEntity  = this.restTemplate.postForEntity("/api/robotPositionsForGivenInstructions", input, String.class);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isEqualTo(output);
    }

    @Test
    public void shouldThrowBadRequestError_whenRequestBodyIsEmpty(){
        String input = "";
        ResponseEntity<String> responseEntity  = this.restTemplate.postForEntity("/api/robotPositionsForGivenInstructions", input, String.class);
		assertThat(responseEntity.getBody()).isNull();
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldThrowBadRequestError_whenRequestBodyIsInvalid(){
        String input = "5 3";
        ResponseEntity<String> responseEntity  = this.restTemplate.postForEntity("/api/robotPositionsForGivenInstructions", input, String.class);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isEqualTo("Input is not in appropriate format");

    }

}
