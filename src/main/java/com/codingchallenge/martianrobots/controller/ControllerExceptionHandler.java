package com.codingchallenge.martianrobots.controller;

import com.codingchallenge.martianrobots.model.MartianDefinedException;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.ExceptionUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({MartianDefinedException.class})
    protected ResponseEntity<Object> handleMartianDefinedException(final MartianDefinedException ex, final WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage() , new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({RuntimeException.class, Exception.class})
    protected ResponseEntity<Object> handleRuntimeException(final RuntimeException ex, final WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage() , new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}
