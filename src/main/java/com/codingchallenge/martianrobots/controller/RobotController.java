package com.codingchallenge.martianrobots.controller;

import com.codingchallenge.martianrobots.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RobotController {

    @Autowired
    private ProcessService processService;
    
    @PostMapping("/robotPositionsForGivenInstructions")
    public String getOutputForInput(@RequestBody String input){
        // validate input by parsing it
        // perform action from the input
        // return output
        return processService.processInput(input);
    }
}
