package com.codingchallenge.martianrobots.service;

import com.codingchallenge.martianrobots.model.*;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class InputParserService {

    Map<String, Boolean> robotScent = new HashMap<>();

    public ParsedInput processInput(String input){
        try{
            String[] lines = input.split("\\n");
            int size = lines.length;

            if(size < 3 || size %2 == 0){
                throw new MartianDefinedException("Input is not correct");
            }
            List<ParsedRobotInput> parsedRobotInputs = new ArrayList<>();
            for(int i= 1; i<size; i+=2){
                parsedRobotInputs.add(new ParsedRobotInput(parseRobot(lines[i]), parseRobotInstructions(lines[i+1])));
            }
            ParsedInput parsedInput = new ParsedInput();
            parsedInput.setWorldEnd(parseCoordinates(lines[0]));
            parsedInput.setRobots(parsedRobotInputs);
            return parsedInput;
        } catch (Exception ex){
            throw new MartianDefinedException("Input is not in appropriate format");
        }

    }

    private Coordinates parseCoordinates(String line){
        String[] worldInput = line.trim().split(" ");
        return new Coordinates(Long.parseLong(worldInput[0]),Long.parseLong(worldInput[1]));
    }

    private Robot parseRobot(String line){
        String[] robotInput = line.trim().split(" ");

        return new Robot(Long.parseLong(robotInput[0]), Long.parseLong(robotInput[1]), robotInput[2].charAt(0));
    }

    private List<Character> parseRobotInstructions(String line){
        String[] instructions = line.trim().split("");
        return Arrays.stream(instructions).map(inst -> inst.charAt(0)).collect(Collectors.toList());
    }

}
