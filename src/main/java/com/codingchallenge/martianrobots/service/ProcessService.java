package com.codingchallenge.martianrobots.service;

import com.codingchallenge.martianrobots.model.Coordinates;
import com.codingchallenge.martianrobots.model.ParsedInput;
import com.codingchallenge.martianrobots.model.Robot;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class ProcessService {

    private final InputParserService inputParserService;

    public ProcessService(InputParserService inputParserService) {
        this.inputParserService = inputParserService;
    }

    public String processInput(String input){
        ParsedInput parsedInput = inputParserService.processInput(input);
        return computeParsedInput(parsedInput);
    }

    private String computeParsedInput(ParsedInput input){
        Coordinates worldEnd = input.getWorldEnd();
        String output = input.getRobots().stream().map(parsedRobotInput -> {
            Robot robot = parsedRobotInput.getRobot();
            parsedRobotInput.getInstructions().forEach(instruction -> {
                if(robot.isLost()){
                    return;
                }
                robot.giveInstruction(instruction, worldEnd);
            });
            return robot.toString();
        }).collect(Collectors.joining("\n"));
        Robot.clearRobotScent();
        return output;
    }

}
