package com.codingchallenge.martianrobots.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigInteger;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Coordinates {
	private long x;
	private long y;

    public boolean checkIfInsideWorld(Coordinates worldVertex){
        if(this.x < 0 || this.x > worldVertex.x || this.y < 0 || this.y > worldVertex.y){
            return false;
        } else {
            return true;
        }
    }

}
