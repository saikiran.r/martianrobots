package com.codingchallenge.martianrobots.model;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MartianDefinedException extends RuntimeException {
    public MartianDefinedException(String message){
        super(message);
        log.error(message, this);
    }

}
