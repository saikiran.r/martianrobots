package com.codingchallenge.martianrobots.model;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Constants {
    public static final List<Character> ORIENTATIONS = Arrays.asList('N', 'E', 'S', 'W');
    public static final Character LEFT = 'L';
    public static final Character RIGHT = 'R';
    public static final Character FORWARD = 'F';
    public static final Set<Character> DIRECTIONS = Set.of(LEFT, RIGHT);

}
