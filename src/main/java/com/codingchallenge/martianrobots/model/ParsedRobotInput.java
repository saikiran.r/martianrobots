package com.codingchallenge.martianrobots.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ParsedRobotInput {
    private Robot robot;
    private List<Character> instructions;
}
