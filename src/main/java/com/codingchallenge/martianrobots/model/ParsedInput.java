package com.codingchallenge.martianrobots.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ParsedInput {

    private Coordinates worldEnd;
    private Coordinates worldStart = new Coordinates(0, 0);
    private List<ParsedRobotInput> robots;

}
