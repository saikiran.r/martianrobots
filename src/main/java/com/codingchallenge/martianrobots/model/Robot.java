package com.codingchallenge.martianrobots.model;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

import static com.codingchallenge.martianrobots.model.Constants.*;

@Slf4j
@Getter
public class Robot {
    private Coordinates position;
    private Character orientation;
    private boolean isLost;
    private static Map<String, Boolean> robotScent = new HashMap<>();

    public Robot(long x, long y, Character orientation){
        this.position = new Coordinates(x, y);

        if(!ORIENTATIONS.contains(orientation)){
            log.info("Direction is invalid for Robot with coordinates {},{}", x, y);
            throw new MartianDefinedException("Direction is invalid");
        }
        this.orientation = orientation;
        this.isLost = false;
    }

    public Robot giveInstruction(Character instruction, Coordinates worldVertex){
        if(this.isLost){
            return this;
        }
        if(DIRECTIONS.contains(instruction)) {
            this.turn(instruction);
        } else if(instruction.equals(FORWARD)){
            this.moveForward(worldVertex);
        } else {
            throw new MartianDefinedException("Invalid Instruction");
        }
        return this;
    }

    private void turn(Character direction){
        int index = ORIENTATIONS.indexOf(this.orientation);
        int length = ORIENTATIONS.size();
        if(direction.equals(LEFT)){
            index = (index - 1 < 0) ? (length - 1) : (index - 1);
        } else {
            index = (index + 1 >= length) ? 0 : (index + 1);
        }
        this.orientation = ORIENTATIONS.get(index);
    }

    private Robot moveForward(Coordinates worldVertex){

        if(this.canSmellScent()){
            return this;
        }

        Coordinates oldPosition = this.position;
        Coordinates newPosition = new Coordinates(oldPosition.getX(), oldPosition.getY());
        int index = ORIENTATIONS.indexOf(this.orientation);
        switch (index){
            case 0: newPosition.setY(oldPosition.getY() + 1); break;
            case 1: newPosition.setX(oldPosition.getX() + 1); break;
            case 2: newPosition.setY(oldPosition.getY() - 1); break;
            case 3: newPosition.setX(oldPosition.getX() - 1); break;
        }

        if(!newPosition.checkIfInsideWorld(worldVertex)){
            this.isLost = true;
            this.leaveScent();
        } else {
            this.position = newPosition;
        }

        return this;
    }

    private String generateScent(){
        Coordinates position = this.position;
        return position.getX() + "-" + position.getY() + "-" + this.orientation;
    }

    private void leaveScent(){
        Robot.robotScent.put(this.generateScent(), true);
    }

    private boolean canSmellScent(){
        return Robot.robotScent.containsKey(this.generateScent());
    }

    public static void clearRobotScent(){
        Robot.robotScent = new HashMap<>();
    }

    @Override
    public String toString() {
        return this.position.getX() +
                " " + this.position.getY() +
                " " + this.orientation +
                (this.isLost? " LOST" : "");
    }
}
