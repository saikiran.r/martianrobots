### Martian Robots Coding challenge (Deployed in Heroku for easy access: https://martianrobots-api.herokuapp.com/ui)

This is build using maven sprint boot and java 11 and React JS. 
This Application has two modules
1. Backend module - This is the backend module code base. This has an API that takes the input and returns the output solving the problem.
2. UI module - The UI module codebase is available in a seperate repo - https://gitlab.com/saikiran.r/martian-robots-app


### How to run the Application
**1. In Local**
- Step 1: clone the git repository
- Step 2: run command 'mvn clean install'
- Step 3: run the command 'mvnw spring-boot:run'

> **or**

**2. Run in remote**
- This application is deployed in Heroku and you could access by using the url https://martianrobots-api.herokuapp.com/ui

----------------------------------------------------------------

### How to hit the API with input and see the output
Step 1: Check if the application is running succesfully or not. When we hit the below url in the browser/postman, we should see the text "Spring is here!"
- Local: http://localhost:8080/
- Remote: https://martianrobots-api.herokuapp.com


******Step 2: Hit the below REST API in postman using POST method and enter your input in the request body to see the output.**
- Local: http://localhost:8080/api/robotPositionsForGivenInstructions
- Remote: https://martianrobots-api.herokuapp.com/robotPositionsForGivenInstructions

******Step 3: See UI by going to the below URL. The same input could be entered by using this GUI to see the output**
- Local: http://localhost:8080/ui
- Remote: https://martianrobots-api.herokuapp.com/ui

-----------------------------------------------------------------

**Input**

- The first line of input is the upper-right coordinates of the rectangular world, the lower-left
coordinates are assumed to be 0, 0.
The remaining input consists of a sequence of robot positions and instructions (two lines per robot).
A position consists of two integers specifying the initial coordinates of the robot and an orientation
(N, S, E, W), all separated by whitespace on one line. A robot instruction is a string of the letters “L”,
“R”, and “F” on one line.
Each robot is processed sequentially, i.e., finishes executing the robot instructions before the next
robot begins execution.
The maximum value for any coordinate is 50.
All instruction strings will be less than 100 characters in length.

**Sample Input**
```
5 3
1 1 E
RFRFRFRF
3 2 N
FRRFLLFFRRFLL
0 3 W
LLFFFLFLFL
```


**Sample Output**
```
1 1 E
3 3 N LOST
2 3 S
```

--------------------------------------------------------------------------

**Tech Debts:**
- Had to write unit tests for services and models. Usually, followed TDD approach initially but has to fallback to manual testing post development due to shortage to time. 


-------------------------------------------------------------------------
Below are a few Improvementsthat could be planned to enhance the application:
1. Write Unit tests for the code. Would be using JEST to write frontend unit tests.
2. Have the frontend and backend in one codebase - By adding the frontend code in the spring boot project.
3. Design the UI by using different colors and layouts. Right now it is very simple and serves the purpose given.
4. Design and build a graphical visualiser of the problem by using canvas or any visualiser tools to draw a rectangular grid and mark robots position in it.

--------------------------------------------------------------------------




### Background

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.



